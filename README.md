<p>Stack: InertiaJS, VueJS3, Laravel, TailwindCSS.</p>

### Online demo
https://newsapi-test.dev.hdd.lt/

### Install (docker)

- Run this to install laravel sail

```
docker run --rm \
-u "$(id -u):$(id -g)" \
-v $(pwd):/var/www/html \
-w /var/www/html \
laravelsail/php80-composer:latest \
composer install --ignore-platform-reqs
```

- run `cp .env.example .env`
- Get API key from https://newsapi.org/ and insert it to NEWS_API_KEY env variable.
- run `./vendor/bin/sail up -d`
- run `./vendor/bin/sail artisan key:generate`
- run `./vendor/bin/sail artisan migrate --seed`
- Project should be accesible via http://localhost
